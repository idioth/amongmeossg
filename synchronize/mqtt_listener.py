import paho.mqtt.client as mqtt
import requests
import time
import json


MOBIUS_SERVER_IP = "3.239.7.234"
MOBIUS_SERVER_PORT = 7579
AE_NAME = "Team_3"

BLOCKCHAIN_SERVER_IP = "54.157.56.166"
BLOCKCHAIN_SERVER_PORT = 8000

headers = {
    'X-M2M-RI': '12345',
    'X-M2M-Origin': 'M2M-Origin',
    'Content-Type': 'application/vnd.onem2m-res+json; ty=4',
    'Cache-Control': 'no-cache',
}


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Bad connection return code=%d", rc)
    else:
        print('connect OK')


def on_disconnect(client, userdata, flags, rc=0):
    print("disconnected", rc)


def on_subscribe(client, userdata, mid, granted_qos):
    pass


def on_message(client, userdata, msg):
    msg = json.loads(msg.payload.decode("utf-8"))
    try:
        content = msg['pc']['m2m:sgn']['nev']['rep']['m2m:cin']['con']
        data = {
            'device_id': content['device-id'],
            'user_id': content['rfid-unique-id'],
            'temperature': str(content['temperature'])
        }
        res = requests.post(f'http://{BLOCKCHAIN_SERVER_IP}:{BLOCKCHAIN_SERVER_PORT}/covid/new', json=data)
        print(data['user_id'], data['temperature'], res.text)

        # save to database
    except:
        return

def discover_all_topics(host, port, ae_name):
    res = requests.get(f'http://{host}:{port}/Mobius/{ae_name}?fu=1&ty=3', headers=headers)
    containers = res.json()['m2m:uril']

    topics = set()

    for container in containers:
        res = requests.get(f'http://{host}:{port}/{container}', headers=headers)
        topics.add(res.json()['m2m:cnt']['cr'])

    return topics



def subscribe_topic(host, port, ae_name, topic):
    resource_name = topic[1:] + "_sub"
    data = {
        "m2m:sub": {
            "rn": resource_name,
            "enc": {
                "net": [3, 4]
            },
            "nu": [f"mqtt://{host}/{resource_name}?ct=json"],
            "exc": 10,
            "pn": "1",
            "cr": topic,
        }
    }

    res = requests.post(f'http://{host}:{port}/Mobius/{ae_name}/sensor', json=data, headers=headers)

    return resource_name


if  __name__ == "__main__":
    topics = discover_all_topics(MOBIUS_SERVER_IP, MOBIUS_SERVER_PORT, AE_NAME)
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe
    client.on_message = on_message

    while True:
        client.connect(MOBIUS_SERVER_IP, 1883)
        for topic in topics:
            sub = subscribe_topic(MOBIUS_SERVER_IP, MOBIUS_SERVER_PORT, AE_NAME, topic)
            client.subscribe(f'/oneM2M/req/Mobius2/{sub}/json', 1)
    
        client.loop_start()
        time.sleep(30)
        client.loop_stop()
        print('loop stop')

