from argparse import ArgumentParser
from pprint import pprint

import paho.mqtt.client as mqtt
import requests
import random
import time


headers = {
    'X-M2M-RI': '12345',
    'X-M2M-Origin': 'M2M-Origin',
    'Content-Type': 'application/vnd.onem2m-res+json; ty=4',
    'Cache-Control': 'no-cache',
}


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Bad connection return code=%d", rc)
    else:
        print('connect OK')


def on_disconnect(client, userdata, flags, rc=0):
    print("disconnected", rc)


def on_subscribe(client, userdata, mid, granted_qos):
    pass


def on_message(client, userdata, msg):
    print(msg.payload.decode("utf-8"))


def discover_all_topics(host, port, ae_name):
    res = requests.get(f'http://{host}:{port}/Mobius/{ae_name}?fu=1&ty=3', headers=headers)
    containers = res.json()['m2m:uril']

    topics = set()

    for container in containers:
        res = requests.get(f'http://{host}:{port}/{container}', headers=headers)
        topics.add(res.json()['m2m:cnt']['cr'])

    return topics


def subscribe_topic(host, port, ae_name, topic):
    resource_name = topic[1:] + "_sub"
    data = {
        "m2m:sub": {
            "rn": resource_name,
            "enc": {
                "net": [3, 4]
            },
            "nu": [f"mqtt://{host}/{resource_name}?ct=json"],
            "exc": 10,
            "pn": "1",
            "cr": topic,
        }
    }

    res = requests.post(f'http://{host}:{port}/Mobius/{ae_name}/sensor', json=data, headers=headers)
    print(res.json())

    return resource_name


def send_dummy_data(host, port, ae_name, interval):
    while True:
        temperature = random.randint(35, 39) + float(random.randint(0, 9) / 10)
        data = {
            "m2m:cin": {
                "cnf": "text/plain:0",
                "con": {
                    'temperature': temperature,
                    'rfid-unique-id': str(random.randint(1000000, 5000000)),
                    'capture': 'https://s3.amazonaws.com/test/image/path',
                    'status': 'NORMAL' if temperature < 37.5 else 'ALERT'
                }
            }
        }

        res = requests.post(f'http://{host}:{port}/Mobius/{ae_name}/sensor', json=data, headers=headers)
        pprint(res.json())

        time.sleep(interval)

    print("Terminating dummy data sender")


def init_script(host, port, ae_name):
    # create ae
    data = {
        'm2m:ae': {
            'rn': ae_name,
            "api": "0.2.481.2.0001.001.000111",
            "lbl": ["sejong", "open-source-sw", "team_3"],
            "rr": True,
            "poa": [f"http://{host}:9727"]
        }
    }
    res = requests.post(f'http://{host}:{port}/Mobius', json=data, headers=headers)
    print('[CREATE AE]', res.status_code)

    if res.status_code != 201:
        return

    # create container
    data = {
        'm2m:cnt': {
            'rn': 'sensor',
            'lbl': ['sensor'],
            'mbs': 16384
        }
    }
    res = requests.post(f'http://{host}:{port}/Mobius/{ae_name}', json=data, headers=headers)
    print('[CREATE CNT]', res.status_code)

    if res.status_code != 201:
        return

    print('Successfully initialize Mobius server')


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-m', '--mode', default='listener', help='Mode of emulation')
    parser.add_argument('-s', '--server', required=True, help='oneM2M Server Host')
    parser.add_argument('-p', '--port', default=7579, type=int, help='oneM2M Server Host')
    parser.add_argument('-a', '--ae', help='Name of Application Entity')
    parser.add_argument('-i', '--interval', default=10, type=int, help='Interval of data publishing')

    args = parser.parse_args()

    if args.mode == 'listener':
        topics = discover_all_topics(args.server, args.port, args.ae)
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        client.on_subscribe = on_subscribe
        client.on_message = on_message

        client.connect(args.server, 1883)
        for topic in topics:
            sub = subscribe_topic(args.server, args.port, args.ae, topic)
            client.subscribe(f'/oneM2M/req/Mobius2/{sub}/json', 1)
        client.loop_forever()

    elif args.mode == 'init':
        init_script(
            host=args.server,
            port=args.port,
            ae_name=args.ae,
        )

    else:
        send_dummy_data(
            host=args.server,
            port=args.port,
            ae_name=args.ae,
            interval=args.interval
        )
