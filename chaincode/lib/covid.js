'use strict';

const { Contract } = require('fabric-contract-api');

class Covid extends Contract {

    async initLedger(ctx) {
        // console.info('============= START : Initialize Ledger ===========');
        // console.info('============= END : Initialize Ledger ===========');
    }

    async queryRecord(ctx, uuidNumber) {
        const dataAsBytes = await ctx.stub.getState(uuidNumber);
	if (!dataAsBytes || dataAsBytes.length === 0) {
	    throw new Error(`${uuidNumber} does not exist`);
	}

	return dataAsBytes.toString();
    }

    async createRecord(ctx, uuidNumber, deviceId, userId, userTemperature, userImage, accessTime) {
        console.info('============= START : Create Covid Record ===========');

	const data = {
            deviceId,
            userId,
            userTemperature,
	    userImage,
            accessTime,
        };

        await ctx.stub.putState(uuidNumber, Buffer.from(JSON.stringify(data)));

	console.info('============= END : Create Covid Record ===========');
    }

    async queryAllData(ctx) {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            allResults.push({id: key, record: record });
        }
        return JSON.stringify(allResults);
    }

    async queryByDevice(ctx, deviceId) {
	const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                if (record.deviceId === deviceId)
                    allResults.push({id: key, record: record});
            } catch (err) {
                console.log(err);
            }
        }
        return JSON.stringify(allResults);
    }

    async queryByHighTemperature(ctx, threshold) {
    	const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                if (record.userTemperature > threshold)
                    allResults.push({id: key, record: record});
            } catch (err) {
                console.log(err);
            }
        }
        return JSON.stringify(allResults);
    }

    async queryByAccessTime(ctx, startTime, endTime) {
	const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                if (record.accessTime >= startTime && record.accessTime <= endTime)
                    allResults.push({id: key, record: record});
            } catch (err) {
                console.log(err);
            }
        }
        return JSON.stringify(allResults);
    }

    async queryByUserId(ctx, userId) {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
	        if (record.userId === userId)
		    allResults.push({id: key, record: record});
            } catch (err) {
                console.log(err);
            }
	}
        return JSON.stringify(allResults);
    }
}

module.exports = Covid;

