from mfrc522 import SimpleMFRC522

import RPi.GPIO as GPIO

import Adafruit_DHT
import requests
import subprocess
import time

sensor = Adafruit_DHT.DHT11
pin = 2
reader = SimpleMFRC522()

THRESHOLD = 37.5
MOBIUS_SERVER_IP = "3.239.7.234"
MOBIUS_SERVER_PORT = 7579
IMAGE_SERVER_PORT = 8080
AE_NAME = "Team_3"
MOBIUS_HEADERS = {
    'X-M2M-RI': '12345',
    'X-M2M-Origin': 'M2M-Origin',
    'Content-Type': 'application/vnd.onem2m-res+json; ty=4',
    'Cache-Control': 'no-cache'
}


def mask_identifier(filename):
    res = requests.post('https://no-more-mask.namjun.kim/detect', files={'image': open(filename, 'rb')})
    return res.json()['mask']


def led(pin, t):
    # GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, True)
    time.sleep(t)
    GPIO.output(pin, False)
    # GPIO.cleanup(pin)


def capture_image():
    proc = subprocess.Popen(['raspistill', '--nopreview', '--timeout', '1', '-q', '75', '-o', 'tmp.jpg'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    return err == ''


def get_temperature():
    _, temperature = Adafruit_DHT.read_retry(sensor, pin)
    return temperature


def send_to_mobius(uid, temperature, capture_url):
    data = {
        "m2m:cin": {
            "cnf": "text/plain:0",
            "con": {
                "temperature": temperature,
                "user_id": uid,
                "capture": upload_image(),
                "status": "PASS" if temperature <= THRESHOLD else "FAIL"
            }
        }
    }
    res = requests.post(f'http://{MOBIUS_SERVER_IP}:{MOBIUS_SERVER_PORT}/Mobius/{AE_NAME}/sensor', json=data, headers=MOBIUS_HEADERS)
    if res.status_code == 201:
        print(uid, 'send success')
    else:
        print(uid, 'send failure')


def upload_image():
    res = requests.post(f'http://{MOBIUS_SERVER_IP}:{IMAGE_SERVER_PORT}/upload', files={'image': open('tmp.jpg', 'rb')})
    return res.text


def collect_info(uid):
    print(int(time.time()), uid, 'door request')

    time.sleep(2)

    temperature = get_temperature() + 10
    print(uid, 'temperature', temperature)

    if temperature >= THRESHOLD:
        print(uid, 'cannot open the door - high temperature')
        led(40, 1)
    else:
        print(uid, 'open the door')
        led(12, 1)

    capture_image()
    print(uid, 'image captured')

    send_to_mobius(uid, temperature, "")


if __name__ == "__main__":
    try:
        while True:
            print('waiting...')
            uid, _ = reader.read()
            collect_info(uid)
    finally:
        GPIO.cleanup()

